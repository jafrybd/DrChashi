<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="“Crop diseases” are a significant threat to food security and food supply chain, but due to the lack of system and infrastructure it was difficult to identify. Since the world is expanding rapidly, so, resolve these kinds of threats we have used smartphones along with machine learning algorithms to identify the damaged crop and suggested the medi kits.">
    <meta name="keywords" content="online disease detection system, plant disease detection, disease detection by machine learning, machine learning software bangladesh, crops disease detection  disease detection in fruits, disease detection system bangladesh,.auto medicine suggestions">
    <meta name="author" content="Medina Tech">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dr. Chashi | Real Time Plant Disease Detection Apps</title>

    <!-- Favicon  -->
    <link rel="icon" href="assets/img/favicon.png">

    <!-- ***** All CSS Files ***** -->

    <!-- Style css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-G6DQKNQH9R"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-G6DQKNQH9R');
    </script>

</head>

<body class="homepage-2">
    <!--====== Scroll To Top Area Start ======-->
    <div id="scrollUp" title="Scroll To Top">
        <i class="fas fa-arrow-up"></i>
    </div>
    <!--====== Scroll To Top Area End ======-->

    <div class="main">
        <!-- ***** Header Start ***** -->
        <header class="navbar navbar-sticky navbar-expand-lg navbar-dark">
            <div class="container position-relative">
                <a class="navbar-brand" href="https://www.drchashi.com">
                    <img class="navbar-brand-regular" src="assets/img/logo/logo.png" alt="brand-logo">
                    <img class="navbar-brand-sticky" src="assets/img/logo/logo.png" alt="sticky brand-logo">
                </a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-inner">
                    <!--  Mobile Menu Toggler -->
                    <button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <nav>
                        <ul class="navbar-nav" id="navbar-nav">
                            <li class="nav-item dropdown">
                                <a data-translatekey="about" class="nav-link scroll" href="#about">
                                    About
                                </a>
                            </li>
                            <li class="nav-item">
                                <a data-translatekey="background" class="nav-link scroll" href="#background">Background</a>
                            </li>
                            <li class="nav-item">
                                <a data-translatekey="features" class="nav-link scroll" href="#features">Features</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a data-translatekey="purpose" class="nav-link scroll" href="#purpose">
                                    Purpose
                                </a>
                            </li>
                            <li class="nav-item">
                                <a data-translatekey="target-users" class="nav-link scroll" href="#target-users">Target Users</a>
                            </li>
                            <li class="nav-item">
                                <a data-translatekey="contact" class="nav-link scroll" href="#contact">Contact</a>
                            </li>

                        </ul>
                        <!-- <div class="others-option"> -->
                        <div class="language_change">
                            <div class="switch">
                                <div class="language">
                                    <input onclick="englishMode()" id="q1" name="locale" type="radio" value="en" checked>
                                    <label for="q1">English</label>
                                </div>
                                <div class="language">
                                    <input onclick="banglaMode()" id="q2" name="locale" type="radio" value="bn">
                                    <label for="q2">বাংলা</label>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </nav>
                </div>
            </div>
        </header>
        <!-- ***** Header End ***** -->

        <!-- ***** Banner Area Start ***** -->
        <section id="about" class="section welcome-area bg-inherit h-100vh overflow-hidden">
            <div class="shapes-container">
                <div class="bg-shape"></div>
            </div>
            <div class="container h-100">
                <div class="row align-items-center h-100">
                    <!-- Welcome Intro Start -->
                    <div class="col-12 col-md-6">
                        <div class="welcome-intro">
                            <h2 data-translatekey="about">About</h2>
                            <p class="my-4 py-3" data-translatekey="about-desc">
                                Machine Learning Technology to perform Image Processing through Mobile & Web Application to detect crop disease & recommend solution for curing disease. The technology works by using a dataset of crop images & connection to a solution product database. With the use of advanced Artificial Intelligence techniques its possible to detect the corresponding diseases by images on real time.
                            </p>
                            <!-- <a href="#" class="btn">Get Started</a> -->
                        </div>
                    </div>
                    <div class="col-12 col-md-5 offset-lg-1">
                        <!-- Welcome Thumb -->
                        <div class="welcome-thumb" data-aos="fade-right" data-aos-delay="500" data-aos-duration="1000">
                            <img src="assets/img/dr-1.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Banner Area End ***** -->

        <!-- ***** Background Area Start ***** -->
        <section id="background" class="section service-area bg-gray overflow-hidden ptb_100">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 data-translatekey="background">Background</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <!-- Service Text -->
                        <div class="service-text pt-4 pt-lg-0 row justify-content-center">
                            <h3 class="text-capitalize mb-4" data-translatekey="problem">Problem</h3>
                            <!-- Service List -->
                            <ul class="service-list">
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <icon-problem>
                                            <i class="fa fa-exclamation" aria-hidden="true"></i>
                                        </icon-problem>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="problem-text-1">Agriculture Industry of Bangladesh contributes 19.6% to the national GDP, providing employment for 63% of Bangladesh’s population.</p>
                                    </div>
                                </li>
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <icon-problem>
                                            <i class="fa fa-exclamation" aria-hidden="true"></i>
                                        </icon-problem>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="problem-text-2">Usually, farmers in Bangladesh apply inorganic fertilizer manually based on their assumption, causing disproportionate use of fertilizer on the plants. Not using any sensors to measure the soil–plant system’s nutrient content before applying the fertilizers causes increased nutrient imbalance, reduced production efficiency, and adverse effects on climate change and public health.</p>
                                    </div>
                                </li>

                            </ul>
                            <!-- <a href="#" class="btn btn-bordered mt-4">Learn More</a> -->
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 row justify-content-center align-content-center">
                        <!-- Service Thumb -->
                        <div class="features-thumb mx-auto">
                            <img src="assets/img/dr-3.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <!-- Discover Text -->
                        <div class="discover-text pt-4 pt-lg-0 row justify-content-center">
                            <h3 class="mb-4 pb-sm-0" data-translatekey="solution">Solution</h3>
                            <!-- <p class="d-none d-sm-block pt-3 pb-4 text-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolor ut iusto vitae autem neque eum ipsam.</p> -->
                            <!-- Check List -->
                            <ul class="check-list">
                                <li class="single-service media py-2 justify-content-center align-items-center">

                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <div class="service-icon pr-4">
                                            <icon-solution>
                                                <i class="fa fa-lightbulb" aria-hidden="true"></i>
                                            </icon-solution>
                                        </div>
                                        <div class="service-text media-body">
                                            <p data-translatekey="solution-text-1">Artificial Intelligence for detection of the diseases with image.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="single-service media py-2 justify-content-center align-items-center">
                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <div class="service-icon pr-4">
                                            <icon-solution>
                                                <i class="fa fa-lightbulb" aria-hidden="true"></i>
                                            </icon-solution>
                                        </div>
                                        <div class="service-text media-body">
                                            <p data-translatekey="solution-text-2">Knowledge & guidance on proper product & amount / dosage of fertilizers, insecticides & pesticides.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="single-service media py-2 justify-content-center align-items-center">
                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <div class="service-icon pr-4">
                                            <icon-solution>
                                                <i class="fa fa-lightbulb" aria-hidden="true"></i>
                                            </icon-solution>
                                        </div>
                                        <div class="service-text media-body">
                                            <p data-translatekey="solution-text-3">Knowledge & guidance in analyzing plant health properly.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="single-service media py-2 justify-content-center align-items-center">
                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <div class="service-icon pr-4">
                                            <icon-solution>
                                                <i class="fa fa-lightbulb" aria-hidden="true"></i>
                                            </icon-solution>
                                        </div>
                                        <div class="service-text media-body">
                                            <p data-translatekey="solution-text-4">Automation to reduce monetary loss caused by mistreatment of plants. Automation for efficiency, health & environment benefits as incentive.</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <!-- <div class="icon-box d-flex mt-3">
                                <div class="service-icon">
                                    <span><svg class="svg-inline--fa fa-bell fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bell" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z"></path></svg></span>
                                </div>
                                <div class="service-icon px-3">
                                    <span><svg class="svg-inline--fa fa-envelope-open fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope-open" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M512 464c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V200.724a48 48 0 0 1 18.387-37.776c24.913-19.529 45.501-35.365 164.2-121.511C199.412 29.17 232.797-.347 256 .003c23.198-.354 56.596 29.172 73.413 41.433 118.687 86.137 139.303 101.995 164.2 121.512A48 48 0 0 1 512 200.724V464zm-65.666-196.605c-2.563-3.728-7.7-4.595-11.339-1.907-22.845 16.873-55.462 40.705-105.582 77.079-16.825 12.266-50.21 41.781-73.413 41.43-23.211.344-56.559-29.143-73.413-41.43-50.114-36.37-82.734-60.204-105.582-77.079-3.639-2.688-8.776-1.821-11.339 1.907l-9.072 13.196a7.998 7.998 0 0 0 1.839 10.967c22.887 16.899 55.454 40.69 105.303 76.868 20.274 14.781 56.524 47.813 92.264 47.573 35.724.242 71.961-32.771 92.263-47.573 49.85-36.179 82.418-59.97 105.303-76.868a7.998 7.998 0 0 0 1.839-10.967l-9.071-13.196z"></path></svg></span>
                                </div>
                                <div class="service-icon">
                                    <span><svg class="svg-inline--fa fa-video fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="video" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M336.2 64H47.8C21.4 64 0 85.4 0 111.8v288.4C0 426.6 21.4 448 47.8 448h288.4c26.4 0 47.8-21.4 47.8-47.8V111.8c0-26.4-21.4-47.8-47.8-47.8zm189.4 37.7L416 177.3v157.4l109.6 75.5c21.2 14.6 50.4-.3 50.4-25.8V127.5c0-25.4-29.1-40.4-50.4-25.8z"></path></svg></span>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Background Area End ***** -->
        <!-- ***** Features Area Start ***** -->
        <section id="features" class="section features-area overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 data-translatekey="features">Features</h2>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-12 col-md-6 col-lg-4">
                        <!-- Features Item -->
                        <ul class="features-item">
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 py-3 py-md-4 wow fadeInLeft" data-aos-duration="2s" data-wow-delay="0.3s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/disease-detection.png" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="disease-detection">Disease Detection</h3>
                                        <p data-translatekey="features-point-1">Real Time Plant Disease Detection via Image Processing</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 py-3 py-md-4 wow fadeInLeft" data-aos-duration="2s" data-wow-delay="0.6s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/solution-prediction.png" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="solution-prediction">Solution Prediction</h3>
                                        <p data-translatekey="features-point-2">Real Time Disease Solution Prediction.</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 py-3 py-md-4 wow fadeInLeft" data-aos-duration="2s" data-wow-delay="0.9s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/recommendation.png" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="recommendation">Recommendation</h3>
                                        <p data-translatekey="features-point-3">Suggest Product & Dosage for curing Plant Disease.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4 d-none d-lg-block">
                        <!-- Featutes Thumb -->
                        <div class="features-thumb text-center row justify-content-center align-self-center">
                            <img src="assets/img/dr-2.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <!-- Features Item -->
                        <ul class="features-item">
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 px-3 py-md-4 wow fadeInRight" data-aos-duration="2s" data-wow-delay="0.3s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/crop-detection.jpg" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="features-point-4">Crops Weed Detection</h3>
                                        <p data-translatekey="crops-detection-desc">Crops Weed Detection.</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 px-3 py-md-4 wow fadeInRight" data-aos-duration="2s" data-wow-delay="0.6s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/monitoring.png" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="monitoring">Monitoring</h3>
                                        <p data-translatekey="features-point-5">Stress and Growth Monitoring.</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Image Box -->
                                <div class="image-box media px-1 px-3 py-md-4 wow fadeInRight" data-aos-duration="2s" data-wow-delay="0.9s">
                                    <!-- Featured Image -->
                                    <div class="featured-img mr-3 row justify-content-center align-self-center">
                                        <img class="avatar-sm" src="assets/img/icon/suggestion.jpg" alt="">
                                    </div>
                                    <!-- Icon Text -->
                                    <div class="icon-text media-body align-self-center align-self-md-start">
                                        <h3 class="mb-2" data-translatekey="suggestion">Suggestion</h3>
                                        <p data-translatekey="features-point-6">Climate Changing Related Suggestion.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Features Area End ***** -->
        <!-- ***** Purpose Start ***** -->
        <section id="purpose" class="section work-area bg-overlay overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Work Content -->
                        <div class="work-content text-center">
                            <h2 class="text-white" data-translatekey="purpose">Purpose</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <!-- Single Work -->
                        <div class="single-work text-center p-3">
                            <!-- Work Icon -->
                            <div class="work-icon">
                                <img class="avatar-md" src="assets/img/icon/mission.png" alt="">
                            </div>
                            <h3 class="text-white py-3" data-translatekey="mission">Mission</h3>
                            <p class="text-white" data-translatekey="mission-text-l1">To provide real time plant disease detection Mobile Application, easy to use by all members of the Agriculture Industry of Bangladesh.
                                To ensure that the solution provides plant disease detection results with the highest accuracy possible. </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <!-- Single Work -->
                        <div class="single-work text-center p-3 margin25">
                            <!-- Work Icon -->
                            <div class="work-icon">
                                <img class="avatar-md" src="assets/img/icon/vission.png" alt="">
                            </div>
                            <h3 class="text-white py-3" data-translatekey="vision">Vision</h3>
                            <p class="text-white" data-translatekey="mission-text-l2">To be the pioneer in Agro-Tech industry of Bangladesh as a power tool for smart farming practices (real-time plant health analysis, plant care resources & support)</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Work Area End ***** -->
        <!-- ***** What Makes sApp Different Area Start ***** -->
        <section id="makes-different" class="section features-area style-two overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 data-translatekey="makes-different">What Makes Us Different?</h2>
                            <!-- <p class="d-none d-sm-block mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.</p> -->
                            <!-- <p class="d-block d-sm-none mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati.</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 res-margin">
                        <!-- Image Box -->
                        <div class="image-box text-center icon-1 padding15 wow fadeInLeft" data-wow-delay="0.4s">
                            <!-- Featured Image -->
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/image-processing.png" alt="">
                            </div>
                            <!-- Icon Text -->
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="image-processing">Image Processing</h4>
                                <p data-translatekey="image-processing-desc">AI based image processing for crops diseases detection.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 res-margin">
                        <!-- Image Box -->
                        <div class="image-box text-center icon-1 wow padding15 fadeInUp" data-wow-delay="0.2s">
                            <!-- Featured Image -->
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/smart-solution.png" alt="">
                            </div>
                            <!-- Icon Text -->
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="smart-solution">Smart Solution</h4>
                                <p data-translatekey="smart-solution-desc">Get the solution immediately with the proper amount of solution product usage.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 res-margin">
                        <!-- Image Box -->
                        <div class="image-box text-center icon-1 wow padding15 fadeInRight" data-wow-delay="0.4s">
                            <!-- Featured Image -->
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/consultation.png" alt="">
                            </div>
                            <!-- Icon Text -->
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="consultation">Consultation</h4>
                                <p data-translatekey="consultation-desc">Crops cultivation consultation for farmers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 res-margin">
                        <!-- Image Box -->
                        <div class="image-box text-center icon-1 wow padding15 fadeInRight" data-wow-delay="0.4s">
                            <!-- Featured Image -->
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/easy-use.png" alt="">
                            </div>
                            <!-- Icon Text -->
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="easy-use">Easy to use</h4>
                                <p data-translatekey="easy-use-desc">Smart and easy to use procedure for implementing good harvesting methodology.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** What Makes sApp Different Area End ***** -->

        <!-- ***** Value Proposition Area Start ***** -->
        <section id="value-proposition" class="section discover-area bg-gray overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 data-translatekey="value-proposition">Value Proposition</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between align-items-center">
                    <div class="col-12 col-lg-5 order-2 order-lg-1">
                        <!-- Discover Thumb -->
                        <!-- <div class="service-thumb discover-thumb mx-auto text-center pt-5 pt-lg-0">
                            <img src="assets/img/discover/thumb-2.png" alt="">
                        </div> -->
                        <div class="service-thumb-user mx-auto justify-content-between align-items-center">
                            <img src="assets/img/user-experience.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-lg-7 order-1 order-lg-2">
                        <!-- Discover Text -->
                        <div class="discover-text px-0 px-lg-4 pt-4 pt-lg-0">
                            <h3 class="pb-4" data-translatekey="user-experience">User Experience</h3>
                            <!-- Check List -->
                            <ul class="check-list">
                                <li class="py-1">
                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <span class="icon align-self-center"><i class="fas fa-check"></i></span>
                                        <span class="media-body pl-3" data-translatekey="environment-desc">Environment friendly & safe option for disease detection, as we allow users to have less contact with infected plants & or, use less chemicals for disease detection.</span>
                                    </div>
                                </li>
                                <li class="py-1">
                                    <!-- List Box -->
                                    <div class="list-box media">
                                        <span class="icon align-self-center"><i class="fas fa-check"></i></span>
                                        <span class="media-body pl-3" data-translatekey="integration-product-desc">Integration to product & disease database to cater to various Agro firms, allowing seamless customized solutions.</span>
                                    </div>
                                </li>
                            </ul>
                            <!-- <div class="icon-box d-flex mt-3">
                                <div class="service-icon">
                                    <span><i class="fas fa-bell"></i></span>
                                </div>
                                <div class="service-icon px-3">
                                    <span><i class="fas fa-envelope-open"></i></span>
                                </div>
                                <div class="service-icon">
                                    <span><i class="fas fa-video"></i></span>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Value Proposition Area End ***** -->

        <!-- ***** Value Proposition Area Start ***** -->
        <section id="data-quality" class="section service-area bg-inherit overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-12 col-lg-6 order-2 order-lg-1">
                        <!-- Service Text -->
                        <div class="service-text pt-4 pt-lg-0">
                            <h3 class="mb-4" data-translatekey="data-quality">Data Quality</h3>
                            <!-- Service List -->
                            <ul class="service-list">
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <span><i class="fab fa-buffer"></i></span>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="dq1-desc">Raw data is continuously & dedicatedly collected by in-house team members & agriculturalists from the field, ensuring best form of captured image to make the best exploratory data analysis using different EDA techniques.</p>
                                    </div>
                                </li>
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <span><i class="fab fa-buffer"></i></span>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="dq2-desc">Bangla & English for text & audio to best serve the audience of Bangladesh.</p>
                                    </div>
                                </li>
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <span><i class="fab fa-buffer"></i></span>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="dq3-desc">Audio guide to help users challenged to read.</p>
                                    </div>
                                </li>
                                <!-- Single Service -->
                                <li class="single-service media py-2">
                                    <div class="service-icon pr-4">
                                        <span><i class="fab fa-buffer"></i></span>
                                    </div>
                                    <div class="service-text media-body">
                                        <p data-translatekey="dq4-desc">Android format, to be available for vast majority of users in Bangladesh at an affordable cost.</p>
                                    </div>
                                </li>
                            </ul>
                            <!-- <a href="#" class="btn btn-bordered mt-4">Learn More</a> -->
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 order-1 order-lg-2 d-none d-md-block">
                        <!-- Service Thumb -->
                        <div class="service-thumb mx-auto">
                            <img src="assets/img/data-quality.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Value Proposition Area End ***** -->

        <!-- ***** Features Area Start ***** -->
        <section id="target-users" class="section bg-gray features-area ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 data-translatekey="target-users">Target Users</h2>
                            <p class="d-none d-sm-block mt-4" data-translatekey="target-users-desc">Local & International Agro Product Firms supplying Fertilizers, Insecticides, Pesticides or any other Smart Farming solutions to Farmers in Bangladesh</p>
                            <!-- <p class="d-none d-sm-block mt-3">Research Institutes, Government Institutions who are equipped with using or providing Smartphones for research or Smart Farming </p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-5">
                        <!-- Icon Box -->
                        <div class="icon-box text-center p-4 wow fadeInUp" data-wow-duration="2s">
                            <!-- Featured Icon -->
                            <div class="targerD3">
                                <div class="row targerD1 justify-content-center align-items-center" data-translatekey="tu1">
                                    1
                                </div>
                                <div class="row targerD2 justify-content-center align-items-center" data-translatekey="tu1-desc">
                                    Farmers & Stakeholders in the Supply Chain
                                </div>
                            </div>
                            <div class="targerD3">
                                <div class="row targerD1 justify-content-center align-items-center" data-translatekey="tu2">
                                    2
                                </div>
                                <div class="row targerD2 justify-content-center align-items-center" data-translatekey="tu2-desc">
                                    Researchers & Government Officials
                                </div>
                            </div>
                            <div class="targerD3">
                                <div class="row targerD1 justify-content-center align-items-center" data-translatekey="tu3">
                                    3
                                </div>
                                <div class="row targerD2 justify-content-center align-items-center" data-translatekey="tu3-desc">
                                    Students & Faculty of Agriculture Universities
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-7">
                        <!-- Icon Box -->
                        <div class="icon-box text-center p-4 wow fadeInUp bdMap" data-wow-duration="2s" data-wow-delay="0.2s">
                            <!-- Featured Icon -->
                            <img src="assets/img/bdMap.svg" alt="BD Map" />
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- ***** Features Area End ***** -->

        <!-- ***** What Makes sApp Different Area Start ***** -->
        <!-- <section id="why-mt" class="section features-area style-two overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <div class="section-heading text-center">
                            <img class="mb-3" src="assets/img/mt-logo.svg" alt="Medina Tech" width="80px">
                            <h2 data-translatekey="why-mt">Why Medina Tech?</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 res-margin">
                        <div class="image-box text-center icon-1 padding15 wow fadeInLeft customHeight" data-wow-delay="0.4s">
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/custom.png" alt="">
                            </div>
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="customizations">Customization </h4>
                                <p data-translatekey="customization-desc">Customizing to provide the best User Experience is a big priority for us, therefore our in house UI/UX Designers, Software Engineers and Data Scientists work together to provide the best customization possible.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 res-margin">
                        <div class="image-box text-center icon-1 wow padding15 fadeInUp customHeight" data-wow-delay="0.2s">
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-sm" src="assets/img/icon/data.png" alt="">
                            </div>
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="data-visualizations">Data Visualization</h4>
                                <p data-translatekey="data-visualization-desc">Agriculturalists can access real-time reports and analytical data, anywhere. Moreover, storing facility is developed using advanced Cloud based platform, to ensure flexibility in terms of the quantity of data they want to store & process. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="image-box text-center icon-1 wow padding15 fadeInRight customHeight" data-wow-delay="0.4s">
                            <div class="featured-img mb-3 marginTop25">
                                <img class="avatar-md-custom" src="assets/img/icon/iaas.png" alt="">
                            </div>
                            <div class="icon-text">
                                <h4 class="mb-2" data-translatekey="iaas">Infrastructure as a Service</h4>
                                <p data-translatekey="iaas-desc">Storing facility is developed using advanced Cloud based platform, to ensure that schools can get flexibility in terms of the quantity of data they want to store & process.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <section class="section download-area overlay-dark ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
                        <!-- Download Text -->
                        <div class="feature-heading text-center whyMT">
                            <a href="https://www.medinatech.co/"><img class="mb-4" src="assets/img/logo_tab - Copy.png" alt="medina logo"></a>
                            <h2 class="text-white" data-translatekey="why-medina-tech">Why Medina Tech?</h2>
                            <h3 class="text-white my-3 d-sm-block" data-translatekey="why-medina-tech-2">
                                Customization | Data Visualization | Infrastructure as a Service
                            </h3>
                            <p class="text-white" data-translatekey="why-medina-tech-3">Customizing to provide the best User Experience is a big priority for us, therefore our in house UI/UX Designers, Software Engineers and Data Scientists work together to provide the best customization possible whether it is in the form of an App or Dashboard. Agriculturalists can access real-time reports and analytical data, anywhere. Moreover, storing facility is developed using advanced Cloud based platform, to ensure flexibility in terms of the quantity of data they want to store & process.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** What Makes sApp Different Area End ***** -->
        <!--====== Contact Area Start ======-->
        <section id="contact" class="contact-area bg-gray subscribe-area ptb_100">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-10 col-lg-8">
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-12 col-md-6 pt-lg-4">

                        <div class="contactImg service-thumb discover-thumb">
<!--                            <img src="assets/img/contact-img.png" alt="">-->
                            <section class="">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <div class="subscribe-content text-center">
                                                <h2>Collaborate with Us</h2>
                                                <p class="my-4">We are open to collaborate with National,International,Government or Private organization for image data collection for our system. Contribution with any type of relevant datasets will highly appreciated. Contact us for further details. </p>
                                                <!-- Subscribe Form -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="col-12 col-md-5 pt-4 pt-md-0">
                        <!-- Contact Box -->
                        <div class="contact-box text-center">
                            <!-- Contact Form -->
                            <form id="contactForm" novalidate="true">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required="required">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                                        </div>
                                        <div class="form-group">
                                            <input type="number" class="form-control" name="phone_number" placeholder="Phone Number" required="required">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="msg_subject" placeholder="Subject" required="required">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" placeholder="Message" required="required"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-lg btn-block mt-3" disabled><span class="text-white pr-3"><i class="fas fa-paper-plane"></i></span>
                                            <font data-translatekey="send-msg">Send Message</font>
                                        </button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                            <p class="form-message"></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--====== Contact Area End ======-->

        <!--====== Height Emulator Area Start ======-->
        <div class="height-emulator d-none d-lg-block"></div>
        <!--====== Height Emulator Area End ======-->

        <!--====== Footer Area Start ======-->
        <footer class="footer-area footer-fixed">
            <!-- Footer Top -->
            <div class="footer-top ptb_100">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <!-- Footer Items -->
                            <div class="footer-items footer-custom">
                                <h3 class="footer-title mb-2 text-center" data-translatekey="developed-by">Developed By</h3>
                                <div class="button-group store-buttons store-black d-flex flex-wraps align-self-center row justify-content-center">
                                    <!-- Logo -->
                                    <a class="navbar-brand footer-logo-custom" href="https://www.medinatech.co/">
                                        <img class="" src="assets/img/mt-2.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <!-- Footer Items -->
                            <div class="footer-items text-center">
                                <!-- Footer Title -->
                                <h3 class="footer-title mb-2" data-translatekey="location">Location</h3>
                                <div class="button-group store-buttons store-black d-flex flex-wrap row footerLocation">

                                    <ul>
                                        <li class="py-2">
                                            <a class="media" href="#">
                                                <span class="media-body align-self-center text-center" data-translatekey="address">House #25, Road #4, Block #F, Banani, Dhaka 1213 </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <!-- Footer Items -->
                            <div class="footer-items text-center">
                                <!-- Footer Title -->
                                <h3 class="footer-title mb-2" data-translatekey="contact">Contact</h3>
                                <div class="button-group store-buttons store-black d-flex flex-wrap row justify-content-center">

                                    <ul>
                                        <li class="py-2">
                                            <a class="media" href="tel:+8809638600700">
                                                <span class="media-body align-self-center text-center" data-translatekey="phone">+88 096 3860 0700</span>
                                            </a>
                                        </li>
                                        <li class="py-2">
                                            <a class="media" href="mailto:support@medinatech.com">
                                                <span class="media-body align-self-center text-center" data-translatekey="email">support@medinatech.com</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="social-icons d-flex justify-content-center">
                                    <a class="facebook" href="https://www.facebook.com/medinatech.co">
                                        <i class="fab fa-facebook-f"></i>
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <!-- Footer Items -->
                            <div class="footer-items text-center">
                                <!-- Footer Title -->
                                <h3 class="footer-title mb-2" data-translatekey="useful-links">Quick Links</h3>
                                <!-- Store Buttons -->
                                <div class="button-group store-buttons store-black d-flex flex-wrap row justify-content-center">
                                    <ul>
                                        <li class="py-2">
                                            <a href="#about" class="scroll" data-translatekey="about">About</a>
                                        </li>
                                        <li class="py-2">
                                            <a href="#features" class="scroll" data-translatekey="features">Features</a>
                                        </li>
                                        <li class="py-2">
                                            <a href="#contact" class="scroll" data-translatekey="contact">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- Copyright Area -->
                            <div class="copyright-area d-flex flex-wrap justify-content-center py-4">
                                <!-- Copyright Left -->
                                <div class="copyright-left"><font data-translatekey="cpy">Copyright © 2020-2021, Medina tech. All Rights Reserved</font>
                                </div>
                                <!-- Copyright Right -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--====== Footer Area End ======-->
    </div>


    <!-- ***** All jQuery Plugins ***** -->

    <!-- jQuery(necessary for all JavaScript plugins) -->
    <script src="assets/js/jquery/jquery.min.js"></script>

    <!-- Bootstrap js -->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Plugins js -->
    <script src="assets/js/plugins/plugins.min.js"></script>

    <!-- Active js -->
    <script src="assets/js/active.js"></script>

    <!-- Language Related JS -->
    <script src="assets/js/tinyi18n.js"></script>
    <script>
        tinyi18n.loadTranslations('translation.json');
    </script>

    <script>
        function englishMode() {
            tinyi18n.setLang('en');

            document.getElementById("tt-en").classList.remove('tt-none')
            document.getElementById("tt-bn").classList.add('tt-none')
        }
    </script>
    <script>
        function banglaMode() {
            tinyi18n.setLang('bd');

            document.getElementById("tt-bn").classList.remove('tt-none')
            document.getElementById("tt-en").classList.add('tt-none')
        }
    </script>
</body>


</html>