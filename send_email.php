<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

//Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);


$errorMSG = "";

// NAME
if (empty($_POST["name"])) {
    $errorMSG = "Name is required ";
} else {
    $name = $_POST["name"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

// Phone
if (empty($_POST["phone_number"])) {
    $errorMSG .= "Phone Number is required ";
} else {
    $phone = $_POST["phone_number"];
}

// MSG SUBJECT
if (empty($_POST["msg_subject"])) {
    $errorMSG .= "Subject is required ";
} else {
    $msg_subject = $_POST["msg_subject"];
}


// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "Message is required ";
} else {
    $message = $_POST["message"];
}

$subject = "Class Connect: Contact Us Message";

// prepare email body text
$body = "";
$body .= "<strong>Name: </strong>";
$body .= '<strong>' . $name . '</strong>';
$body .= "<br/>";
$body .= "<strong>Email: </strong>";
$body .= $email;
$body .= "<br/>";
$body .= "<strong>Phone: </strong>";
$body .= $phone;
$body .= "<br/>";
$body .= "<strong>Subject: </strong>";
$body .= $msg_subject;
$body .= "<br/>";
$body .= "<strong>Message: </strong>";
$body .= $message;
$body .= "<br/>";

try {
    //Server settings
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host = 'email-smtp.ap-south-1.amazonaws.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth = true;                                   //Enable SMTP authentication
    $mail->Username = 'AKIAXXK2576WYDXLLTKX';                     //SMTP username
    $mail->Password = 'BP4q/ejsgn76H+IAuspmJqRKmWOvDZ7OVVUlJzhkXhtc';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('support@medinatech.co', 'Medina Tech');
    $mail->addAddress('support@medinatech.co', 'Medina Tech');     //Add a recipient


    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body = $body;

    $success = $mail->send();

    if ($success && $errorMSG == "") {
        echo "success";
    } else {
        if ($errorMSG == "") {
            echo "Something went wrong :(";
        } else {
            echo $errorMSG;
        }
    }

} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
